# Project description
This is a simple application for Pizzeria implemented in JavaScript. Frontend implemented by HTML/SCSS.
# Project objective
The main purpose of this project was to play a little bit with JavaScript, HTML and SCSS.
# Test it live
The application has been deployed on [Heroku](https://www.heroku.com/) cloud . You can test it by yourself on this [website](https://pizzeria-853582.herokuapp.com).