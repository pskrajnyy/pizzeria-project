import { settings, select, classNames } from './settings.js';
import Product from './components/Product.js';
import Cart from './components/Cart.js';
import Booking from './components/Booking.js';

const app = {
  initPages: function () {
    const thisApp = this;
    const idFromHash = window.location.hash.replace('#/', '');

    thisApp.pages = document.querySelector(select.containerOf.pages).children;
    thisApp.nav = document.querySelector(select.nav.navbar);
    thisApp.navLinks = document.querySelectorAll(select.nav.links);
    thisApp.imageBoxes = document.querySelectorAll('.navi a');

    let pageMatchingHash = thisApp.pages[0].id;

    for (let page of thisApp.pages) {
      if (page.id == idFromHash) {
        pageMatchingHash = page.id;
        break;
      }
    }

    thisApp.activatePage(pageMatchingHash);

    for (let link of thisApp.navLinks) {
      link.addEventListener('click', function (event) {
        event.preventDefault();
        const clickedElement = this;
        const id = clickedElement.getAttribute('href').replace('#', '');

        thisApp.activatePage(id);

        window.location.hash = '#/' + id;
      });
    }

    for (let box of thisApp.imageBoxes) {
      box.addEventListener('click', function (event) {
        const clickedElement = this;
        event.preventDefault();
        const id = clickedElement.getAttribute('href').replace('#', '');
        thisApp.activatePage(id);
      });
    }
  },

  activatePage: function (pageId) {
    const thisApp = this;

    if (pageId == 'home') {
      thisApp.nav.classList.add(classNames.nav.hide);
    } else {
      thisApp.nav.classList.remove(classNames.nav.hide);
    }

    for (let page of thisApp.pages) {
      page.classList.toggle(classNames.pages.active, page.id == pageId);
    }

    for (let link of thisApp.navLinks) {
      link.classList.toggle(classNames.nav.active, link.getAttribute('href') == '#' + pageId);
    }
    document.body.classList = pageId;
  },

  initBooking: function () {
    const thisApp = this;
    const bookingElem = document.querySelector(select.containerOf.booking);
    thisApp.booking = new Booking(bookingElem);
  },

  initData: function () {
    const thisApp = this;

    thisApp.data = {};
    const url = settings.db.url + '/' + settings.db.product;

    fetch(url)
      .then(function (rawResponse) {
        return rawResponse.json();
      })
      .then(function (parsedResponse) {
        thisApp.data.products = parsedResponse;
        thisApp.initMenu();
      });
  },

  initMenu: function () {
    const thisApp = this;

    for (let productData in thisApp.data.products) {
      new Product(thisApp.data.products[productData].id, thisApp.data.products[productData]);
    }
  },

  initCart: function () {
    const thisApp = this;
    const cartElem = document.querySelector(select.containerOf.cart);
    thisApp.cart = new Cart(cartElem);

    thisApp.productList = document.querySelector(select.containerOf.menu);

    thisApp.productList.addEventListener('add-to-cart', function (event) {
      app.cart.add(event.detail.product);
    });
  },

  initCarousel: function () {
    /* global Mustache */

    const appContainer = document.querySelector('#carousel');
    const template = document.querySelector('#template_carousel').innerHTML;
    let id = 0;

    const data = {
      carousel: [
        {
          title: 'AMAZING SERVICE!',
          content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam egestas viverra tortor, eu ullamcorper dui imperdiet nec. Nunc sed dolor at elit lobortis sodales.',
          author: '- Margaret Osborne'
        },
        {
          title: 'Not neapolitan pizza, but still good',
          content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Earum vero ipsam magni blanditiis laudantium porro, natus aliquid necessitatibus beatae deleniti.',
          author: '- Ozzy Osborne'
        },
        {
          title: 'Very tasty.',
          content: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur quia ab, vero nam magnam velit molestias quos amet quidem quaerat rem alias a. Id quam, cupiditate praesentium maxime tempora facere?',
          author: '- Pizza Lover'
        }
      ],
      idx: () => id++
    };

    const outputHTML = Mustache.render(template, data);
    appContainer.innerHTML = outputHTML;

    const items = document.querySelectorAll('.slide');
    const links = document.querySelectorAll('.carousel-dots-item');

    items[0].classList.add('active');
    links[0].classList.add('active');

    for (let link of links) {
      link.addEventListener('click', e => {
        const element = e.currentTarget;
        const index = element.getAttribute('data-index');

        changeSlide(index);
      });
    }

    function changeSlide(id) {



      for (let item of items) {
        item.classList.remove('active');
      }

      for (let l of links) {
        l.classList.remove('active');
      }

      items[id].classList.add('active');
      links[id].classList.add('active');
    }

    setInterval(function() {
      id++;

      if (id >= data.carousel.length) {
        id = 0;
      }
      changeSlide(id);
    }, 4000);
  },



  init: function () {
    const thisApp = this;

    thisApp.initPages();
    thisApp.initData();
    thisApp.initCart();
    thisApp.initBooking();
    thisApp.initCarousel();
  },
};

app.init();
