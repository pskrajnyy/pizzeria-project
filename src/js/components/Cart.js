import { settings, select, classNames, templates } from '../settings.js';
import CartProduct from './CartProduct.js';
import utils from '../utils.js';

class Cart {
  constructor(element) {
    const thisCart = this;

    thisCart.products = [];
    thisCart.deliveryFee = settings.cart.zeroDeliveryFee;
    thisCart.getElements(element);
    thisCart.initActions();
    thisCart.update();
  }

  getElements(element) {
    const thisCart = this;

    thisCart.dom = {};
    thisCart.dom.wrapper = element;
    thisCart.dom.toggleTrigger = thisCart.dom.wrapper.querySelector(select.cart.toggleTrigger);
    thisCart.dom.productList = thisCart.dom.wrapper.querySelector(select.cart.productList);
    thisCart.dom.form = thisCart.dom.wrapper.querySelector(select.cart.form);
    thisCart.dom.phone = thisCart.dom.wrapper.querySelector(select.cart.phone);
    thisCart.dom.address = thisCart.dom.wrapper.querySelector(select.cart.address);
    thisCart.renderTotalsKeys = ['totalNumber', 'totalPrice', 'subtotalPrice', 'deliveryFee'];

    for (let key of thisCart.renderTotalsKeys) {
      thisCart.dom[key] = thisCart.dom.wrapper.querySelectorAll(select.cart[key]);
    }
  }

  initActions() {
    const thisCart = this;

    thisCart.dom.toggleTrigger.addEventListener('click', function () {
      thisCart.dom.wrapper.classList.toggle(classNames.cart.wrapperActive);
    });

    thisCart.dom.productList.addEventListener('updated', function () {
      thisCart.update();
    });

    thisCart.dom.productList.addEventListener('remove', function () {
      thisCart.remove(event.detail.cartProduct);
    });

    thisCart.dom.productList.addEventListener('edit', function () {
      thisCart.edit(event.detail.cartProduct);
    });

    thisCart.dom.form.addEventListener('submit', function () {
      event.preventDefault();
      if(thisCart.validForm()) {
        thisCart.sendOrder();
      }
    });
  }

  add(menuProduct) {
    const thisCart = this;
    const generatedHTML = templates.cartProduct(menuProduct);
    const generatedDOM = utils.createDOMFromHTML(generatedHTML);

    thisCart.deliveryFee = settings.cart.defaultDeliveryFee;
    thisCart.dom.productList.appendChild(generatedDOM);
    thisCart.products.push(new CartProduct(menuProduct, generatedDOM));
    thisCart.update();
  }

  update() {
    const thisCart = this;
    thisCart.totalNumber = 0;
    thisCart.subtotalPrice = 0;

    for (let product of thisCart.products) {
      thisCart.subtotalPrice += product.price;
      thisCart.totalNumber += product.amount;
    }
    thisCart.totalPrice = thisCart.subtotalPrice + thisCart.deliveryFee;

    for (let key of thisCart.renderTotalsKeys) {
      for (let elem of thisCart.dom[key]) {
        elem.innerHTML = thisCart[key];
      }
    }
  }

  remove(cartProduct) {
    const thisCart = this;
    const index = thisCart.products.indexOf(cartProduct);

    thisCart.products.splice(index, 1);
    cartProduct.dom.wrapper.remove();

    if(!thisCart.products.length > 0) {
      thisCart.deliveryFee = settings.cart.zeroDeliveryFee;
    }
    thisCart.update();
  }

  edit(cartProduct) {
    const thisCart = this;
    thisCart.remove(cartProduct);
  }

  validForm() {
    const thisCart = this;
    const phone = thisCart.dom.phone.value;
    const address = thisCart.dom.address.value;
    const numbers = /^[0-9]+$/;

    if( address == '' || phone == '' || phone.length > 9 || !phone.match(numbers)) {
      alert('Please write correct value in form');
      return false;
    }

    return true;
  }

  sendOrder() {
    let thisCart = this;
    const url = settings.db.url + '/' + settings.db.order;
    const productsSize = thisCart.products.length;

    const payload = {
      address: thisCart.dom.address.value,
      phone: thisCart.dom.phone.value,
      totalPrice: thisCart.totalPrice,
      number: thisCart.totalNumber,
      subtotalPrice: thisCart.subtotalPrice,
      delivery: thisCart.deliveryFee,
      products: [],
    };

    for (const product of thisCart.products) {
      product.getData();
      payload.products.push(product);
    }

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    };

    fetch(url, options)
      .then(function (response) {
        return response.json();
      }).then(function () {
        thisCart.dom.address.value = null;
        thisCart.dom.phone.value = null;

        for (let i = 0; i < productsSize; i++) {
          thisCart.remove(thisCart.products[0]);
        }
        thisCart = thisCart.update();
      });
  }
}

export default Cart;
